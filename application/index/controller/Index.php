<?php
namespace app\index\controller;
use think\Request;
use think\Db;
use think\Controller;
// use Illuminate\Http\Request;
class Index extends Controller
{
  public function index()
  {
    $data = ['user_name' => 'wzp', 'password' => '12346'];
    return Db::table('user')->insert($data);
    // return '<style type="text/css">*{ padding: 0; margin: 0; } .think_default_text{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:)</h1><p> ThinkPHP V5<br/><span style="font-size:30px">十年磨一剑 - 为API开发设计的高性能框架</span></p><span style="font-size:22px;">[ V5.0 版本由 <a href="http://www.qiniu.com" target="qiniu">七牛云</a> 独家赞助发布 ]</span></div><script type="text/javascript" src="https://tajs.qq.com/stats?sId=9347272" charset="UTF-8"></script><script type="text/javascript" src="https://e.topthink.com/Public/static/client.js"></script><think id="ad_bd568ce7058a1091"></think>';
  }
  // 登录验证
  public function login()
  {   
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Headers: Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE');
    header('Access-Control-Max-Age: 1728000');
    $request = Request::instance();
    $method = $request->method();//获取上传方式
    if ($method !== "post" && $method !== "POST") {
      return 100; // 参数错误
    }
    $post = $request->post();//获取post上传的内容
    $username = $post['username'];
    $user_info = Db::table('user')->where('user_name', $username)->select();
    // $sql = Db::table('user')->getLastSql(); // 打印sql 语句
    // echo $sql;
    if (!sizeof($user_info)) {
      return 121; // 121 用户不存在或已删除
    } else if (sha1($user_info[0]['password']) === $post['password']){
      return 0;
    } else {
      return 123; // 123 密码不正确
    }
    // return urldecode(json_encode($user_info));
  }
  // 图片上传
  public function upload() {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Headers: Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE');
    header('Access-Control-Max-Age: 1728000');

    // 获取表单上传文件
    $files = request()->file('image');
    foreach($files as $file){
      // 移动到框架应用根目录/uploads/ 目录下
      $info = $file->validate(['size'=>15678,'ext'=>'jpg,png,gif'])->move( '../uploads');
      if($info){
        // 成功上传后 获取上传信息
        // 输出 jpg
        echo $info->getExtension(); 
        // 输出 42a79759f284b767dfcb2a0197904287.jpg
        echo $info->getFilename(); 
      }else{
        // 上传失败获取错误信息
        echo $file->getError();
      }
    }
  }
}
